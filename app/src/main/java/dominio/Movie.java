package dominio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by PABLO on 25/05/2016.
 */
public class Movie implements Parcelable {
    private static final ArrayList<String> wordsToIgnore =
            new ArrayList<String>(Arrays.asList(new String[]{"da", "de", "do", "dos", "das", "em", "no", "nos",
            "na", "nas", "com", "sem", "a", "as", "o", "os", "aos", "ao", "até", "por", "filme", "que",
            "suas", "sua", "seu", "seus", "e", "pela", "ele", "ela"}));

    private String nome;
    private int ano;
    private String pais;
    private ArrayList<String> generos;
    private String sinopse;
    private ArrayList<String> produtoras;
    private ArrayList<String> palavrasChave;
    private ArrayList<Person> elenco;
    private String poster;

    public Movie() {
        this.nome = "";
        this.ano = 1900;
        this.pais = "";
        this.generos = new ArrayList<>();
        this.sinopse = "";
        this.produtoras = new ArrayList<>();
        this.palavrasChave = new ArrayList<>();
        this.elenco = new ArrayList<>();
        this.poster = "noimage";
    }

    public Movie(String nome, int ano, String pais, ArrayList<String> generos,
                 String sinopse, ArrayList<String> produtoras, ArrayList<String> palavrasChave,
                 ArrayList<Person> elenco, String poster) {
        this.nome = nome;
        this.ano = ano;
        this.pais = pais;
        this.generos = generos;
        this.sinopse = sinopse;
        this.produtoras = produtoras;
        this.palavrasChave = palavrasChave;
        this.elenco = elenco;
        this.poster = poster;
    }

    public Movie(String nome, int ano, String pais, ArrayList<String> generos,
                 String sinopse, ArrayList<String> produtoras, ArrayList<String> palavrasChave,
                 ArrayList<Person> elenco) {
        this.nome = nome;
        this.ano = ano;
        this.pais = pais;
        this.generos = generos;
        this.sinopse = sinopse;
        this.produtoras = produtoras;
        this.palavrasChave = palavrasChave;
        this.elenco = elenco;
        this.poster = "noimage";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public ArrayList<String> getGeneros() {
        return generos;
    }

    public void setGeneros(ArrayList<String> generos) {
        this.generos = generos;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public ArrayList<String> getProdutoras() {
        return produtoras;
    }

    public void setProdutoras(ArrayList<String> produtoras) {
        this.produtoras = produtoras;
    }

    public ArrayList<String> getPalavrasChave() {
        return palavrasChave;
    }

    public void setPalavrasChave(ArrayList<String> palavrasChave) {
        this.palavrasChave = palavrasChave;
    }

    public ArrayList<Person> getElenco() {
        return elenco;
    }

    public void setElenco(ArrayList<Person> elenco) {
        this.elenco = elenco;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public ArrayList<String> getTags() {
        ArrayList<String> tags = new ArrayList<>();

        tags.add(Integer.toString(this.ano));
        tags.add(this.pais);
        tags.addAll(arrayStringFilter(generos));
        tags.addAll(arrayStringFilter(produtoras));
        tags.addAll(arrayStringFilter(palavrasChave));
        // Adiciona nome e sobrenome dos participantes
        for (Person p: elenco) {
            tags.addAll(arrayStringFilter(p.getNames()));
        }
        tags.addAll(tagGenerator(sinopse));
        tags.addAll(tagGenerator(nome));

        return tags;
    }

    /**
     * Separa a string em substrings e retira as palavras que devem ser ignoradas
     * na comparacao com a informacao do filme
     *
     * @param stringArray
     * @return Lista de palavras significativas
     */
    public ArrayList<String> tagGenerator(String stringArray) {
        String stringFiltered = stringFilter(stringArray);
        String[] stringSplitted = stringFiltered.split(" ");
        ArrayList<String> stringSplittedArray = new ArrayList<String>(Arrays.asList(stringSplitted));
        stringSplittedArray.removeAll(wordsToIgnore);

        return stringSplittedArray;
    }

    private String stringFilter(String entrada) {
        String saida = entrada;

        saida = saida.replace(',', '\u0020');
        saida = saida.replace('.', '\u0020');
        saida = saida.replace(';', '\u0020');
        saida = saida.replace(':', '\u0020');
        saida = saida.replace('<', '\u0020');
        saida = saida.replace('>', '\u0020');
        saida = saida.replace('/', '\u0020');
        saida = saida.replace('?', '\u0020');
        saida = saida.replace('_', '\u0020');
        saida = saida.replace('-', '\u0020');
        saida = saida.replace('!', '\u0020');
        saida = saida.replace(')', '\u0020');
        saida = saida.replace('(', '\u0020');
        saida = saida.replace('á', 'a');
        saida = saida.replace('à', 'a');
        saida = saida.replace('ã', 'a');
        saida = saida.replace('â', 'a');
        saida = saida.replace('é', 'e');
        saida = saida.replace('è', 'e');
        saida = saida.replace('ê', 'e');
        saida = saida.replace('í', 'i');
        saida = saida.replace('ó', 'o');
        saida = saida.replace('õ', 'o');
        saida = saida.replace('ô', 'o');
        saida = saida.replace('ú', 'u');
        saida = saida.replace('ü', 'u');
        saida = saida.replace('ç', 'c');
        saida = saida.replace("  ", " ");
        saida = saida.trim();
        saida = saida.toLowerCase();

        return saida;
    }

    public ArrayList<String> arrayStringFilter (ArrayList<String> entrada) {
        ArrayList<String> saida = new ArrayList<>();

        for(String s : entrada) {
            saida.add(stringFilter(s));
        }

        return saida;
    }

    public String getListOfStars () {
        String stars = "";

        for(Person p : getElenco()) {
            if(p.getFuncao().compareToIgnoreCase("ator") == 0) {
                stars += p.getNome() + "\n";
            }
        }

        return stars;
    }

    public String getListOfGenders () {
        String genders = "";

        for(String s : getGeneros()) {
            genders += s + "\n";
        }

        return genders;
    }

    protected Movie(Parcel in) {
        nome = in.readString();
        ano = in.readInt();
        pais = in.readString();
        poster = in.readString();
        if (in.readByte() == 0x01) {
            generos = new ArrayList<String>();
            in.readList(generos, String.class.getClassLoader());
        } else {
            generos = null;
        }
        sinopse = in.readString();
        if (in.readByte() == 0x01) {
            produtoras = new ArrayList<String>();
            in.readList(produtoras, String.class.getClassLoader());
        } else {
            produtoras = null;
        }
        if (in.readByte() == 0x01) {
            palavrasChave = new ArrayList<String>();
            in.readList(palavrasChave, String.class.getClassLoader());
        } else {
            palavrasChave = null;
        }
        if (in.readByte() == 0x01) {
            elenco = new ArrayList<Person>();
            in.readList(elenco, Person.class.getClassLoader());
        } else {
            elenco = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeInt(ano);
        dest.writeString(pais);
        dest.writeString(poster);
        if (generos == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(generos);
        }
        dest.writeString(sinopse);
        if (produtoras == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(produtoras);
        }
        if (palavrasChave == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(palavrasChave);
        }
        if (elenco == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(elenco);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

}
