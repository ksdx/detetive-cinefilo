package dominio;

import java.util.Date;

/**
 * Created by PABLO on 06/11/2016.
 */

public class Query {

    public static String[] COLUMNS = new String[] {"ID", "QUERY", "FOUNDED", "MOVIE", "SEARCH_DATE", "FOUNDED_DATE"};
    private int id;
    private String query;
    private boolean founded;
    //id do imdb
    private String movie;
    private String searchDate;
    private String foundedDate;

    public Query() {
        this.query = "";
        this.founded = false;
        this.movie = "";
        this.searchDate = "";
        this.foundedDate = "";
    }

    public Query(String query) {
        this.query = query;
        this.founded = false;
        this.movie = "";
        this.searchDate = new Date().toString();
        this.foundedDate = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isFounded() {
        return founded;
    }

    public String getFoundedDate() {
        return foundedDate;
    }

    public void setFoundedDate(String foundedDate) {
        this.foundedDate = foundedDate;
    }

    public void setFounded(boolean founded) {
        this.founded = founded;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}
