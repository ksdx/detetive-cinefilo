package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import dominio.Query;
import helper.QueryDbHelper;

/**
 * Created by PABLO on 06/11/2016.
 */

public class QueryDao {
    private SQLiteDatabase bd;

    public QueryDao(Context context) {
        QueryDbHelper queryDbHelper = new QueryDbHelper(context);
        bd = queryDbHelper.getWritableDatabase();
    }

    public void insertOrUpdate(Query query, boolean update) {
        ContentValues values = new ContentValues(5);
        values.put("QUERY", query.getQuery());
        values.put("FOUNDED", (query.isFounded() ? 1 : 0));
        values.put("MOVIE", query.getMovie());
        values.put("SEARCH_DATE", query.getSearchDate());
        values.put("FOUNDED_DATE", query.getFoundedDate());

        if(query.getId() > 0 && update) {
            bd.update("QUERY", values, "id = ?", new String[]{"" + query.getId()});
        } else {
            bd.insert("QUERY", null, values);
        }
    }

    public void remove(Query query) {
        String[] id = {String.valueOf(query.getId())};
        bd.delete("QUERY", "ID = ?", id);
    }

    public List<Query> list() {
        List<Query> querys = new ArrayList<Query>();
        Cursor c = bd.query("QUERY", Query.COLUMNS, null, null, null, null, "SEARCH_DATE");
        if(c.moveToFirst()) {
            do{
                Query query = new Query();
                query.setId(c.getInt(0));
                query.setQuery(c.getString(1));
                query.setFounded((c.getInt(2) == 1 ? true : false));
                query.setMovie(c.getString(3));
                query.setSearchDate(c.getString(4));
                query.setFoundedDate(c.getString(5));

                querys.add(query);
            }while(c.moveToNext());
        }
        c.close();
        return querys;
    }

    public Query findByPrimaryKey(int id) {
        Query query= new Query();

        Cursor cursor = bd.query("QUERY", Query.COLUMNS, "id = "+id, null, null, null, null);

        if(cursor.moveToFirst()) {
            query.setId(cursor.getInt(0));
            query.setQuery(cursor.getString(1));
            query.setFounded((cursor.getInt(2) == 1 ? true : false));
            query.setMovie(cursor.getString(3));
            query.setSearchDate(cursor.getString(4));
            query.setFoundedDate(cursor.getString(5));
        }
        cursor.close();
        return query;
    }
}
