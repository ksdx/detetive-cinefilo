package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import br.com.compaty.detetivecinefilo.R;
import dominio.Movie;
import util.MyVolleySingleton;

/**
 * Created by PABLO on 29/05/2016.
 */
public class MovieAdapter extends ArrayAdapter<Movie> {
    private final Context context;
    ArrayList<Movie> movies = new ArrayList<>();
    ImageLoader mImageLoader;
    NetworkImageView mNetworkImageView;


    public MovieAdapter(Context context, ArrayList<Movie> movies) {
        super(context, R.layout.movie_layout, movies);
        this.context = context;
        this.movies = movies;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.movie_layout, parent, false);

        TextView movieTitle = (TextView) rowView.findViewById(R.id.movietitle);

        Movie movie = movies.get(position);

        mImageLoader = MyVolleySingleton.getInstance(context).getImageLoader();

        mNetworkImageView = (NetworkImageView) rowView.findViewById(R.id.poster);
        mNetworkImageView.setDefaultImageResId(R.drawable.noimage);
        mNetworkImageView.setAdjustViewBounds(true);
        mNetworkImageView.setImageUrl("https://image.tmdb.org/t/p/w500"+movie.getPoster(), mImageLoader);

        movieTitle.setText(movie.getNome());

        return rowView;
    }

}
