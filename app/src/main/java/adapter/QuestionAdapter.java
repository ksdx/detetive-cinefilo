package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.compaty.detetivecinefilo.R;
import dominio.Query;

/**
 * Created by PABLO on 05/06/2016.
 */
public class QuestionAdapter extends ArrayAdapter<Query> {

    //ver recycleview
    static class ViewHolder {
        TextView questionTxt;
        TextView searchdateTxt;
    }

    public QuestionAdapter(Context context, int textViewResourceId, List<Query> questions) {
        super(context, textViewResourceId, questions);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = null;

        if(view == null) {
            LayoutInflater inflater = (LayoutInflater)  getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.question_layout, null);
            holder = new ViewHolder();

            holder.questionTxt = (TextView)  view.findViewById(R.id.question);
            holder.searchdateTxt = (TextView) view.findViewById(R.id.searchdate);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Query query = getItem(position);
        if(query != null) {
            holder.questionTxt.setText(query.getQuery());
            holder.searchdateTxt.setText(query.getSearchDate());
        }
        return view;
    }
}
