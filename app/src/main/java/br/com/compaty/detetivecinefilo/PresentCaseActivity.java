package br.com.compaty.detetivecinefilo;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import dominio.Movie;

public class PresentCaseActivity extends Fragment {

    private RequestQueue queue;
    private EditText queryTxt;
    private Button searchBtn;
    private String url;
    String entrada;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_presentcase, container, false);

        queryTxt = (EditText) view.findViewById(R.id.textoEntrada);
        searchBtn = (Button) view.findViewById(R.id.searchWS);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchws();
            }
        });

        return view;
    }

    public void searchws() {
        queue = Volley.newRequestQueue(getContext());

        entrada = queryTxt.getText().toString().replace(" ", "%20");

        url = "http://10.0.2.2:8080/DetetiveCinefiloRest/service/moviefinder/buscarfilme/"+entrada;

        try {
            JsonArrayRequest jsArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        Gson gson = new Gson();

                        ArrayList<Movie> listOfMovies = new ArrayList<>();

                        for (int i = 0; i < response.length(); ++i) {
                            Movie movie = gson.fromJson(response.getJSONObject(i).toString(), Movie.class);
                            listOfMovies.add(movie);
                        }

                        Intent intent = new Intent(getContext(), MovieListActivity.class);
                        intent.putExtra("question", entrada);
                        intent.putParcelableArrayListExtra("listmovies", listOfMovies);

                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(),
                            url, Toast.LENGTH_LONG)
                            .show();
                }
            });

            jsArrayRequest.setTag("REST");

            queue.add(jsArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        super.onResume();
        ((Main2Activity) getActivity()).getSupportActionBar().setTitle("Busca de Filmes");
    }
}
