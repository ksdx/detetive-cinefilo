package br.com.compaty.detetivecinefilo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

public class NotifSettingsFragment extends Fragment {

    private Switch notifSwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notif_settings, container, false);

        notifSwitch = (Switch) view.findViewById(R.id.notifswitch);

        SharedPreferences sharedPref = getActivity().getSharedPreferences("br.com.compaty.NOTIF_PREFERENCE", Context.MODE_PRIVATE);

        boolean notif = sharedPref.getBoolean("notification", false);

        notifSwitch.setChecked(notif);
        Log.d("PREFERENCE", String.valueOf(notif));

        notifSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if(isChecked) {
                    SharedPreferences sharedPref = getActivity().getSharedPreferences("br.com.compaty.NOTIF_PREFERENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("notification", true);
                    editor.commit();
                    Log.d("NOTIF", String.valueOf(sharedPref.getBoolean("notification", false)));
                } else {
                    SharedPreferences sharedPref = getActivity().getSharedPreferences("br.com.compaty.NOTIF_PREFERENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("notification", false);
                    editor.commit();
                    Log.d("NOTIF", String.valueOf(sharedPref.getBoolean("notification", false)));
                }

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public void onResume() {
        super.onResume();
        ((Main2Activity) getActivity()).getSupportActionBar().setTitle("Configurações");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SharedPreferences sharedPref = getActivity().getSharedPreferences("br.com.compaty.NOTIF_PREFERENCE", Context.MODE_PRIVATE);
        Log.d("NOTIF DESTROYVIEW", String.valueOf(sharedPref.getBoolean("notification", false)));
    }

}
