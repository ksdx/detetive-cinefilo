package br.com.compaty.detetivecinefilo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import adapter.QuestionAdapter;
import dao.QueryDao;
import dominio.Query;

import static android.app.Activity.RESULT_OK;

public class InvestigateMovieActivity extends Fragment {
    private QuestionAdapter adapterList;
    private List<Query> queryList;
    private ActionMode mActionMode;
    private ShareActionProvider mShareActionProvider;
    private static final int EDITAR = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_investigate_movie, container, false);

        QueryDao queryDao = new QueryDao(getContext());
        queryList = queryDao.list();

        ListView listQuerys = (ListView) view.findViewById(R.id.listQuestions);
        adapterList = new QuestionAdapter(getContext(), R.layout.question_layout, queryList);
        listQuerys.setAdapter(adapterList);

        registerForContextMenu(listQuerys);

        listQuerys.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mActionMode != null) {
                    return false;
                }

                // Start the CAB using the ActionMode.Callback defined above
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                mActionMode = activity.startSupportActionMode(mActionModeCallback);
                // Posicao da question na lista
                mActionMode.setTag(i);
                view.setSelected(true);
                return true;
            }
        });

        return view;
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);

            MenuItem shareItem = menu.findItem(R.id.action_share);
            mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
            MenuItemCompat.setActionProvider(shareItem, mShareActionProvider);
            return true;
        }

        // Call to update the share intent
        private void setShareIntent(Intent shareIntent) {
            if (mShareActionProvider != null) {
                mShareActionProvider.setShareIntent(shareIntent);
            }
        }

        private void shareQuestion(String question) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Ajude-me a encontrar este filme:\n" + question);
            shareIntent.setType("text/plain");
            setShareIntent(shareIntent);
            startActivity(shareIntent);
        }

        private void editQuestion(String question, int position) {
            Intent intent = new Intent(getContext(), MovieEditActivity.class);
            intent.putExtra("question", question);
            intent.putExtra("position", position);
            startActivityForResult(intent, EDITAR);
        }

        private void removeQuestion(int position) {
            createAlert(position).show();
        }

        private Dialog createAlert(final int position) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Deseja realmente remover esta busca?")
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            QueryDao queryDao = new QueryDao(getContext());
                            Query queryOld = queryList.get(position);
                            queryDao.remove(queryOld);
                            adapterList.remove(queryOld);
                        }
                    })
                    .setNegativeButton("Não",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            return builder.create();
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }


        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int position = Integer.parseInt(mode.getTag().toString());
            Query question = queryList.get(position);

            switch (item.getItemId()) {
                case R.id.action_share:
                    shareQuestion(question.getQuery());
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.action_edit:
                    editQuestion(question.getQuery(), position);
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.action_remove:
                    removeQuestion(position);
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK) {
            String question = data.getStringExtra("question");
            int position = data.getIntExtra("position", 0);

            QueryDao queryDao = new QueryDao(getContext());
            Query queryOld = queryList.get(position);

            queryOld.setQuery(question);

            queryDao.insertOrUpdate(queryOld, true);

            adapterList.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
