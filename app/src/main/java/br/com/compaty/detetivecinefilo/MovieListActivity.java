package br.com.compaty.detetivecinefilo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import adapter.MovieAdapter;
import dao.QueryDao;
import dominio.Movie;
import dominio.Query;

public class MovieListActivity extends AppCompatActivity {
    private String question;
    ArrayList<Movie> movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_movies);

        ListView list = (ListView) findViewById(R.id.listMovies);

        registerForContextMenu(list);

        Intent intent = getIntent();
        question = intent.getStringExtra("question");
        movieList = intent.getParcelableArrayListExtra("listmovies");

        TextView questionText = (TextView) findViewById(R.id.question);

        questionText.setText("Busca: " + question.replace("%20", " "));

        if(movieList.isEmpty()) {
            addQ(this.getCurrentFocus());
        } else {
            MovieAdapter adapterList = new MovieAdapter(this, movieList);

            list.setAdapter(adapterList);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    Movie movie = movieList.get(position);

                    Intent intent = new Intent(MovieListActivity.this, MovieDetailsActivity.class);

                    intent.putExtra("movie", movie);

                    startActivity(intent);
                }
            });
        }

    }

    public void addQ(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseja que outras pessoas encontrem o filme para você?")
                .setTitle("Filme não encontrado")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Query query = new Query(question.replace("%20", " "));

                        QueryDao queryDao = new QueryDao(getApplicationContext());
                        queryDao.insertOrUpdate(query, false);
                        Toast.makeText(getApplicationContext(), "Busca adicionada no histórico.", Toast.LENGTH_SHORT).show();
                        MovieListActivity.this.finish();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        MovieListActivity.this.finish();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
