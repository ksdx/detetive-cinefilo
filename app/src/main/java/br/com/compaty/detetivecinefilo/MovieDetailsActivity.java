package br.com.compaty.detetivecinefilo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import dominio.Movie;
import util.MyVolleySingleton;

public class MovieDetailsActivity extends AppCompatActivity {

    ImageLoader mImageLoader;
    NetworkImageView mNetworkImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        Intent intent = getIntent();
        Movie movie = intent.getParcelableExtra("movie");

        mImageLoader = MyVolleySingleton.getInstance(getApplicationContext()).getImageLoader();

        mNetworkImageView = (NetworkImageView) findViewById(R.id.moviePoster);
        mNetworkImageView.setDefaultImageResId(R.drawable.noimage);
        mNetworkImageView.setAdjustViewBounds(true);
        mNetworkImageView.setImageUrl("https://image.tmdb.org/t/p/w500"+movie.getPoster(), mImageLoader);

        TextView mvTitle = (TextView) findViewById(R.id.mvtitle);
        TextView movieStars = (TextView) findViewById(R.id.movieStars);
        TextView movieGenders = (TextView) findViewById(R.id.movieGenders);
        TextView movieCountry = (TextView) findViewById(R.id.movieCountry);
        TextView movieDescription = (TextView) findViewById(R.id.movieDescription);

        mvTitle.setText(movie.getNome());
        movieStars.setText(movie.getListOfStars());
        movieGenders.setText(movie.getListOfGenders());
        movieCountry.setText(movie.getPais() + "\n");
        movieDescription.setText(movie.getSinopse());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.movie_found:
                confirmMovie();

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void confirmMovie() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Este é o filme que você procurava?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "Filme encontrado!", Toast.LENGTH_LONG).show();
                        MovieDetailsActivity.this.finish();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        MovieDetailsActivity.this.finish();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
