package br.com.compaty.detetivecinefilo;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NotificationService extends IntentService {

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences("br.com.compaty.NOTIF_PREFERENCE", Context.MODE_PRIVATE);

        while(true) {
            boolean notifOn = sharedPref.getBoolean("notification", false);
            if(notifOn) {
                try {
                    Thread.sleep(10000);
                    Intent notificationIntent = new Intent("br.com.compaty.notification");

                    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this).
                            setSmallIcon(R.mipmap.ic_launcher).
                            setContentTitle("Filme encontrado!").
                            setContentText("Veja se o filme que você procurava foi encontrado!").
                            setVibrate(new long[]{100, 250}).
                            setContentIntent(contentIntent);

                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(1, builder.build());
                    Thread.sleep(10000);
                    mNotificationManager.cancelAll();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
