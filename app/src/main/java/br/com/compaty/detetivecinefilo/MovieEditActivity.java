package br.com.compaty.detetivecinefilo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MovieEditActivity extends AppCompatActivity {
    private EditText question;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_edit);

        question = (EditText) findViewById(R.id.editQuestion);

        Intent intent = getIntent();

        question.setText(intent.getStringExtra("question"));
        position = intent.getIntExtra("position", 0);

    }

    public void editQuestion(View v) {
        createAlert().show();
    }

    private Dialog createAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseja realmente editar?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String q = question.getText().toString();
                        Intent result = new Intent();
                        result.putExtra("question", q);
                        result.putExtra("position", position);
                        setResult(RESULT_OK, result);
                        finish();
                    }
                })
                .setNegativeButton("Não",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        return builder.create();
    }
}
