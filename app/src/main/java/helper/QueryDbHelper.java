package helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PABLO on 06/11/2016.
 */

public class QueryDbHelper extends SQLiteOpenHelper {
    public static final String NOME_BANCO = "querys";
    public static final int VERSAO_BANCO = 1;
    public static final String QUERY_TABLE = "CREATE TABLE QUERY ( " +
            "ID INTEGER NOT NULL PRIMARY KEY autoincrement, " +
            "QUERY TEXT, " +
            "FOUNDED INTEGER, " +
            "MOVIE TEXT, " +
            "SEARCH_DATE TEXT, " +
            "FOUNDED_DATE TEXT);";

    public QueryDbHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(QUERY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS QUERY");
        onCreate(sqLiteDatabase);
    }
}
